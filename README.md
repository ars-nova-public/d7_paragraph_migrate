# d7_paragraph_migrate

Drupal 7 site created to demonstrate migrating paragraph content from D7 to D8.

Works in conjunction with [d8_paragraph_migrate site](https://gitlab.com/ars-nova-public/d8_paragraph_migrate).

Included is a bare-bones Drupal 7 site with:

- two **paragraph** bundles (Text / Text and Text / Image)
- **product** content type with two paragraph fields, allowing for unlimited number of paragraphs of either paragraph bundle
- two product nodes with a mixture of paragraph content
- initial database that populates automatically on startup

## Requirements

Uses [docker4drupal](https://github.com/wodby/docker4drupal).

Requires:

- docker
- docker-compose

## Usage

##### Prep

- add `127.0.0.1 d7.paragraph-migrate.local` to /etc/hosts

##### Directory structure

```
d7_d8_paragraphs/
|
|-- d7_paragraph_migrate/     (d7 site)
|
|-- d8_paragraph_migrate/     (d8 site)
|
\-- volumes/                  (created automatically by docker)
    |-- mariadb/
    \-- mariadb_d8/
```

##### Install

```
# if parent directory doesn't exist
mkdir d7_d8_paragraphs;

cd d7_d8_paragraphs;
git clone git@gitlab.com:ars-nova-public/d7_paragraph_migrate.git
cd d7_paragraph_migrate;
make up;
```

Visit [http://d7.paragraph-migrate.local](http://d7.paragraph-migrate.local). Credentials: admin/admin.
